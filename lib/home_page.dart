import 'package:app_heuristics/cadastroHeuristica_page.dart';
import 'package:flutter/material.dart';

import 'consultaHeuristicasUsuario.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          child: Column(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text('Maria Joana'),
            accountEmail: Text('maria@ab.com'),
          ),
          ListTile(
            leading: Icon(Icons.search),
            title: Text('Busque Famosas Heurísticas'),
            onTap: () {
              print('home');
            },
          ),
          ListTile(
            leading: Icon(Icons.search),
            title: Text('Busque Heurísticas que você cadastrou!'),
            onTap: () {
              print('home');
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return ConsultaHeuristicaUsuario();
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.add),
            title: Text('Cadastre sua heurística'),
            onTap: () {
              print('home');
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return CadastroHeuristicaPage();
              }));
            },
          ),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Revisar sua heurística'),
            onTap: () {
              print('home');
            },
          ),
          ListTile(
            leading: Icon(Icons.delete),
            title: Text('Apagar sua heurística'),
            onTap: () {
              print('home');
            },
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Logout'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
        ],
      )),
      appBar: AppBar(
        title: Text('Menu'),
      ),
      body: new Container(
        color: Colors.grey[200],
        child: new Image.asset('assets/images/IA_home.jpg'),
        alignment: Alignment.centerLeft,
      ),
    );
  }
}
