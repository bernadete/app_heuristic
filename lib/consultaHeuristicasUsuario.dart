import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_heuristics/cadastroHeuristica_page.dart';

class ConsultaHeuristicaUsuario extends StatelessWidget {
  final List<Heuristica> _heuristicas = [];

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text('Consulte suas Heurísticas'),),
        body: ListView.builder(
          itemCount: _heuristicas.length,
          // ignore: missing_return
          itemBuilder: (context, indice) {
            final heuristica = _heuristicas[indice];
            return ItemHeuristica(heuristica);
          },
      ),
        floatingActionButton: FloatingActionButton(onPressed: () {
        final Future<Heuristica> future = Navigator.push(context, MaterialPageRoute(builder: (context) {
        return CadastroHeuristicaPage();
        }));
        future.then((heuristicaRecebida) {
          debugPrint('Chegou no then do future');
          debugPrint('$heuristicaRecebida');
          debugPrint('Criando heuristica');
          _heuristicas.add(heuristicaRecebida);
        });
        },
        child: Icon(Icons.add)),
    ),
    );
  }
}

class ItemHeuristica extends StatelessWidget {

  final Heuristica _heuristica;

  ItemHeuristica(this._heuristica);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        leading: Icon(Icons.edit),
        title: Text(_heuristica.nomeHeuristica),
        subtitle: Text(_heuristica.categoriaHeuristica),
      ),
    );
  }
}


