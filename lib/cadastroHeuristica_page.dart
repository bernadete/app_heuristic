import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CadastroHeuristicaPage extends StatefulWidget {
  @override
  State<CadastroHeuristicaPage> createState() {
    return CadastroHeuristicaPageState();
  }
}

class CadastroHeuristicaPageState extends State<CadastroHeuristicaPage> {
  final TextEditingController _controladorNomeHeuristica =
      TextEditingController();
  final TextEditingController _controladorCategoriaHeuristica =
      TextEditingController();
  final TextEditingController _controladorDescricaoHeuristica =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Nova Heurística'),
        ),
        body: ListView(
          children: <Widget>[
            Editor(
                controlador: _controladorNomeHeuristica,
                rotulo: "Nome da Heurística",
                dica: "Fácil memorização"),
            Editor(
                controlador: _controladorCategoriaHeuristica,
                rotulo: "Categoria da Heurística",
                dica: "Categoria"),
            Editor(
                controlador: _controladorDescricaoHeuristica,
                rotulo: "Descrição da Heurística",
                dica: "Detalhes da Heurística"),
            ElevatedButton(
              child: Text('Cadastrar'),
              onPressed: () {
                _cadastraHeuristica(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  void _cadastraHeuristica(BuildContext context) {
    final String nomeHeuristica = _controladorNomeHeuristica.text;
    final String categoriaHeuristica = _controladorCategoriaHeuristica.text;
    final String descricaoHeuristica = _controladorDescricaoHeuristica.text;
    final novaHeuristica =
        Heuristica(nomeHeuristica, categoriaHeuristica, descricaoHeuristica);
    print('$novaHeuristica');
    Navigator.pop(context, novaHeuristica);
  }
}

class Editor extends StatelessWidget {
  final TextEditingController controlador;
  final String rotulo;
  final String dica;

  Editor({this.controlador, this.rotulo, this.dica});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextField(
        controller: controlador,
        style: TextStyle(fontSize: 24.0),
        decoration: InputDecoration(labelText: rotulo, hintText: dica),
      ),
    );
  }
}

class Heuristica {
  final String nomeHeuristica;
  final String categoriaHeuristica;
  final String descricaoHeuristica;

  Heuristica(
      this.nomeHeuristica, this.categoriaHeuristica, this.descricaoHeuristica);

  @override
  String toString() {
    return 'Heuristica{nomeHeuristica: $nomeHeuristica, categoriaHeuristica: $categoriaHeuristica, descricaoHeuristica: $descricaoHeuristica}';
  }
}
