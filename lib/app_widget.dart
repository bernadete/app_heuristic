import 'package:app_heuristics/cadastroHeuristica_page.dart';
import 'package:app_heuristics/home_page.dart';
import 'package:flutter/material.dart';
import 'consultaHeuristicasUsuario.dart';
import 'login_page.dart';

class AppWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData (primarySwatch: Colors.lightBlue),
        initialRoute: '/consulta',
        routes: {
          '/': (context) => LoginPage(),
          '/home': (context) => HomePage(),
          '/cadastro': (context) => CadastroHeuristicaPage(),
          '/consulta': (context) => ConsultaHeuristicaUsuario()
        }
    );
  }
}