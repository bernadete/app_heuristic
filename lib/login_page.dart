import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  var email = '';
  var senha = '';

  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 310,
                height: 200,
                child: Image.asset('assets/images/image_login.jpg'),
              ),
              Container(height: 20),
              TextField(
                onChanged: (text) {
                  email = text;
                },
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'email',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 10),
              TextField(
                  onChanged: (text) {
                    senha = text;
                  },
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'senha',
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  if(email=='maria@ab.com' && senha == '123'){
                    debugPrint('entrou');
                    Navigator.of(context).pushReplacementNamed('/home');
                  } else {
                    debugPrint('Login inválido!');
                  }
                },
                child: Text('Entrar')
              )
            ],
          ),
        ),
      ),
    ),
    );
  }
}
